﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evaluator
{
    public class Status
    {
        public string Contest { get; set; }
        public string Contestant { get; set; }
        public string Problem { get; set; }
        public double Time { get; set; }
        public long Memory { get; set; }
        public int Score { get; set; }
        public string Message { get; set; }
        public string InputFileName { get; set; }
        public bool IsCorrect { get; set; }
        
        public Status(double time, long memory, int score, string message, string inputFileName, bool isCorrect)
        {
            Time = time;
            Memory = memory;
            Score = score;
            Message = message;
            InputFileName = inputFileName;
            IsCorrect = isCorrect;
        }

        public Status(string contest, string contestant, string problem, double time, long memory, int score, string message, string inputFileName, bool isCorrect)
        {
            Contest = contest;
            Contestant = contestant;
            Problem = problem;
            Time = time;
            Memory = memory;
            Score = score;
            Message = message;
            InputFileName = inputFileName;
            IsCorrect = isCorrect;
        }   
    }
}
