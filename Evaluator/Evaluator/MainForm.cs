﻿using Evaluator.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Evaluator
{
    public partial class MainForm : Form
    {
        private Microsoft.Win32.RegistryKey key;
        private string PascalCompiler;
        private string GPPCompiler;
        private string EvaluationPath;
        private string ApiUrl;
        private string ApiKey;
        
        private List<Status> statuses;
        private List<Judge.Notification> notifications;
        private List<Judge.EvaluationResultEventArgs> evaluationResults;

        public MainForm()
        {
            InitializeComponent();
            key = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(Constants.Evaluator);
            statuses = new List<Status>();
            notifications = new List<Judge.Notification>();
            evaluationResults = new List<Judge.EvaluationResultEventArgs>();
            statusBindingSource.DataSource = statuses;
            notificationsBindingSource.DataSource = notifications;
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            key.Close();
            bgWEvaluator.Dispose();
        }

        private void btnStartEval_Click(object sender, EventArgs e)
        {
            if (!AreSetSettings())
            {
                MessageBox.Show(Constants.CaptionError, Constants.ErrorSettings);
                SettingsForm sf = new SettingsForm();
                sf.ShowDialog();
                return;
            }
            if (bgWEvaluator.IsBusy)
            {
                bgWEvaluator.CancelAsync();
            }
            else
            {
                GetSettings();
                bgWEvaluator.RunWorkerAsync();
                btnStartEval.Text = "Stop";
            }
        }

        #region MainMenu
        private void compilerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SettingsForm sf = new SettingsForm();
            sf.ShowDialog();
        }

        private void howToUseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Constants.HowToUse);
        }

        private void contactToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Constants.Contact);
        }
        #endregion        

        #region BackGroundWorkerEvaluator

        private void bgWEvaluator_DoWork(object sender, DoWorkEventArgs e)
        {
            var worker = sender as BackgroundWorker;
            try
            {
                Judge.Judge judge = new Judge.Judge(ApiUrl, ApiKey, EvaluationPath, GPPCompiler, PascalCompiler);
                judge.TestEvaluated += new Judge.Judge.TestEvaluatedEvent(TestEvaluated);
                judge.JudgeNotification += new Judge.Judge.JudgeNotificationEvent(JudgeNotification);
                judge.Synchronize();
                Thread.Sleep(3000);
                while (!worker.CancellationPending)
                {
                    judge.Evaluate();
                    judge.UpdateResults();
                    judge.SynchronizeSubmissions();
                    LogResults();
                    ClearResults();
                    Thread.Sleep(5000);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                AddNotification(new Judge.Notification(ex.Message, DateTime.Now));
            }            
        }

        private void bgWEvaluator_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }

        private void bgWEvaluator_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            btnStartEval.Text = "Start";
            MessageBox.Show("Evaluation is stopped!");
        }

        #endregion        

        #region Methods
        private void TestEvaluated(object sender, Judge.EvaluationResultEventArgs args)
        {
            SetContestName(args.Contest.Name);
            SetContestantName(args.Contestant.Username);
            SetProblemName(args.Problem.Name);
            Status status = new Status(args.Contest.Name, args.Contestant.Username, args.Problem.Name, (double)args.Result.Time, (long)args.Result.Memory, args.Result.Score, args.Status.Name, args.Test.Name + Constants.In, args.Result.Score != 0);
            SetEvaluationInfo(status);
            AddEvaluationResult(args);
        }

        private void JudgeNotification(object clock, Judge.JudgeNotificationEventArgs args)
        {
            AddNotification(args.Notification);
        }

        private bool AreSetSettings()
        {
            return key.GetValue(Constants.GPPCompiler) != null
                   && key.GetValue(Constants.PascalCompiler) != null
                   && key.GetValue(Constants.ApiUrl) != null
                   && key.GetValue(Constants.ApiKey) != null
                   && key.GetValue(Constants.EvaluationPath) != null;
        }

        private void GetSettings()
        {
            PascalCompiler = key.GetValue(Constants.PascalCompiler).ToString();
            GPPCompiler = key.GetValue(Constants.GPPCompiler).ToString();
            EvaluationPath = key.GetValue(Constants.EvaluationPath).ToString();
            ApiUrl = key.GetValue(Constants.ApiUrl).ToString();
            ApiKey = key.GetValue(Constants.ApiKey).ToString();
        }

        delegate void SetTextCallback(string text);

        private void SetProblemName(string name)
        {
            if (this.lblProblemValue.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetProblemName);
                this.Invoke(d, new object[] { name });
            }
            else
            {
                this.lblProblemValue.Text = name;
            }
        }

        private void SetContestantName(string name)
        {
            if (this.lblContestantName.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetContestantName);
                this.Invoke(d, new object[] { name });
            }
            else
            {
                this.lblContestantName.Text = name;
            }
        }

        private void SetContestName(string name)
        {
            if (this.lblContestName.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetContestName);
                this.Invoke(d, new object[] { name });
            }
            else
            {
                this.lblContestName.Text = name;
            }
        }

        delegate void AddNotificationCallback(Judge.Notification notification);

        private void AddNotification(Judge.Notification notification)
        {
            if (this.notificationsGrid.InvokeRequired)
            {
                AddNotificationCallback d = new AddNotificationCallback(AddNotification);
                this.Invoke(d, new object[] { notification });
            }
            else
            {
                notificationsBindingSource.Add(notification);
                notificationsGrid.Refresh();
                notificationsGrid.FirstDisplayedScrollingRowIndex = notificationsGrid.RowCount - 1;
            }
        }

        delegate void SetStatusCallback(Status status);

        private void SetEvaluationInfo(Status status)
        {
            if (this.gridResults.InvokeRequired)
            {
                SetStatusCallback d = new SetStatusCallback(SetEvaluationInfo);
                this.Invoke(d, new object[] { status });
            }
            else
            {
                statuses.Add(status);
                gridResults.RefreshDataSource();
                gridViewResults.MoveLast();
            }
        }

        delegate void AddEvaluationResultCallback(Judge.EvaluationResultEventArgs evaluationResult);

        private void AddEvaluationResult(Judge.EvaluationResultEventArgs evaluationResult)
        {
            if (this.gridResults.InvokeRequired)
            {
                AddEvaluationResultCallback d = new AddEvaluationResultCallback(AddEvaluationResult);
                this.Invoke(d, new object[] { evaluationResult });
            }
            else
            {
                evaluationResults.Add(evaluationResult);
            }
        }

        delegate void SetResultsCallback();

        private void ClearResults()
        {
            if (this.gridResults.InvokeRequired)
            {
                SetResultsCallback d = new SetResultsCallback(ClearResults);
                this.Invoke(d, new object[] { });
            }
            else
            {
                statuses.Clear();
                evaluationResults.Clear();
                gridResults.RefreshDataSource();
            }
        }

        delegate void LogResultsCallback();

        private void LogResults()
        {
            if (this.gridResults.InvokeRequired)
            {
                LogResultsCallback d = new LogResultsCallback(LogResults);
                this.Invoke(d, new object[] { });
            }
            else
            {
                try
                {
                    if (statuses.Count > 0)
                    {
                        string filePath = EvaluationPath + "\\evaluation report - " + String.Format("{0:dd-MM-yyyy HH-mm-ss}", DateTime.Now) + ".pdf";
                        gridResults.ExportToPdf(filePath);
                        Thread.Sleep(1000);
                    }                    
                }
                catch (Exception)
                {

                }
            }
        }        
        #endregion
    }
}
