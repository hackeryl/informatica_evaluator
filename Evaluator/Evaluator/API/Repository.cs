﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evaluator.API
{
    public class Repository
    {
        public IEnumerable<API.Models.Contest> Contests { get; set; }
        public IEnumerable<API.Models.Contestant> Contestants { get; set; }
        public IEnumerable<API.Models.Problem> Problems { get; set; }
        public IEnumerable<API.Models.Submission> Submissions { get; set; }
        public IEnumerable<API.Models.Status> Statuses { get; set; }
        public IEnumerable<API.Models.Test> Tests { get; set; }
        public List<API.Models.SubmissionResult> SubmissionsResults { get; set; }
    }
}
