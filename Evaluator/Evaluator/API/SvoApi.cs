﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Evaluator.API
{
    public class SvoApi
    {        
        #region Constants
        private static string GET = "Get";
        private static string POST = "Post";
        private static string POSTAsync = "PostAsync";
        private static string DELETE = "DELETE";
        private static string ContestsController = "Contests/";
        private static string ContestantsController = "Contestants/";
        private static string ProblemsController = "Problems/";
        private static string SolutionsController = "Solutions/";
        private static string TestsController = "Tests/";
        private static string StatusesController = "Statuses/";
        private static string ResultsController = "Results/";
        #endregion

        private static HttpClient client = new HttpClient();

        private SvoApi()
        {            
        }

        public static void SetUrl(string url)
        {
            try
            {                
                client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/octet-stream"));
            }
            catch (Exception)
            {
            }            
        }

        #region Contests
        public class Contests
        {            
            public static IEnumerable<Models.Contest> Get()
            {
                List<Models.Contest> result = new List<Models.Contest>();
                try
                {
                    HttpResponseMessage response = client.GetAsync(ContestsController + GET).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        result = response.Content.ReadAsAsync<List<Models.Contest>>().Result;
                    }
                }
                catch (Exception)
                {
                }
                return result;
            }

            public static Models.Contest Get(int id)
            {
                Models.Contest result = null;
                try
                {
                    HttpResponseMessage response = client.GetAsync(ContestsController + GET + "?id=" + id).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        result = response.Content.ReadAsAsync<Models.Contest>().Result;
                    }
                }
                catch (Exception)
                {
                }
                return result;
            }            
        }
        #endregion

        #region Contestants
        public class Contestants
        {
            public static IEnumerable<Models.Contestant> Get()
            {
                List<Models.Contestant> result = new List<Models.Contestant>();
                try
                {
                    HttpResponseMessage response = client.GetAsync(ContestantsController + GET).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        result = response.Content.ReadAsAsync<List<Models.Contestant>>().Result;
                    }
                }
                catch (Exception)
                {
                }
                return result;
            }

            public static Models.Contestant Get(int id)
            {
                Models.Contestant result = null;
                try
                {
                    HttpResponseMessage response = client.GetAsync(ContestantsController + GET + "?id=" + id).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        result = response.Content.ReadAsAsync<Models.Contestant>().Result;
                    }
                }
                catch (Exception)
                {
                }
                return result;
            }
        }
        #endregion

        #region Problems
        public class Problems
        {
            public static IEnumerable<Models.Problem> Get()
            {
                List<Models.Problem> result = new List<Models.Problem>();
                try
                {
                    HttpResponseMessage response = client.GetAsync(ProblemsController + GET).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        result = response.Content.ReadAsAsync<List<Models.Problem>>().Result;
                    }
                }
                catch (Exception)
                {
                }
                return result;
            }

            public static IEnumerable<Models.Problem> GetByContestId(int idContest)
            {
                List<Models.Problem> result = new List<Models.Problem>();
                try
                {
                    HttpResponseMessage response = client.GetAsync(ProblemsController + "GetByContestId?id=" + idContest).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        result = response.Content.ReadAsAsync<List<Models.Problem>>().Result;
                    }
                }
                catch (Exception)
                {
                }
                return result;
            }

            public static Models.Problem Get(int id)
            {
                Models.Problem result = null;
                try
                {
                    HttpResponseMessage response = client.GetAsync(ProblemsController + GET + "?id=" + id).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        result = response.Content.ReadAsAsync<Models.Problem>().Result;
                    }
                }
                catch (Exception)
                {
                }
                return result;
            }
        }
        #endregion

        #region Statuses
        public class Statuses
        {
            public static IEnumerable<Models.Status> Get()
            {
                List<Models.Status> result = new List<Models.Status>();
                try
                {
                    HttpResponseMessage response = client.GetAsync(StatusesController + GET).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        result = response.Content.ReadAsAsync<List<Models.Status>>().Result;
                    }
                }
                catch (Exception)
                {
                }
                return result;
            }

            public static Models.Status Get(int id)
            {
                Models.Status result = null;
                try
                {
                    HttpResponseMessage response = client.GetAsync(StatusesController + GET + "?id=" + id).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        result = response.Content.ReadAsAsync<Models.Status>().Result;
                    }
                }
                catch (Exception)
                {
                }
                return result;
            }
        }
        #endregion

        #region Results
        public class Results
        {
            public static IEnumerable<Models.SubmissionResult> Get()
            {
                List<Models.SubmissionResult> result = new List<Models.SubmissionResult>();
                try
                {
                    HttpResponseMessage response = client.GetAsync(ResultsController + GET).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        result = response.Content.ReadAsAsync<List<Models.SubmissionResult>>().Result;
                    }
                }
                catch (Exception)
                {
                }
                return result;
            }

            public static Models.SubmissionResult Get(int id)
            {
                Models.SubmissionResult result = null;
                try
                {
                    HttpResponseMessage response = client.GetAsync(ResultsController + GET + "?id=" + id).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        result = response.Content.ReadAsAsync<Models.SubmissionResult>().Result;
                    }
                }
                catch (Exception)
                {
                }
                return result;
            }

            public static bool Post(IEnumerable<Models.SubmissionResult> results)
            {
                try
                {
                    HttpResponseMessage response = client.PostAsJsonAsync(ResultsController + POST, results).Result;
                    return response.IsSuccessStatusCode;
                }
                catch (Exception)
                {
                    return false;
                }
            }

            public static bool PostAsync(IEnumerable<Models.SubmissionResult> results)
            {
                try
                {
                    HttpResponseMessage response = client.PostAsJsonAsync(ResultsController + POSTAsync, results).Result;
                    return response.IsSuccessStatusCode;
                }
                catch (Exception)
                {
                    return false;
                }
            }

            public static bool Delete(int id)
            {
                try
                {
                    HttpResponseMessage response = client.DeleteAsync(ResultsController + DELETE + "?id=" + id).Result;
                    return response.IsSuccessStatusCode;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }
        #endregion

        #region Solutions
        public class Solutions
        {
            public static IEnumerable<Models.Submission> Get()
            {
                List<Models.Submission> result = new List<Models.Submission>();
                try
                {
                    HttpResponseMessage response = client.GetAsync(SolutionsController + GET).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        result = response.Content.ReadAsAsync<List<Models.Submission>>().Result;
                    }
                }
                catch (Exception)
                {
                }
                return result;
            }

            public static IEnumerable<Models.Submission> GetUnevaluated()
            {
                List<Models.Submission> result = new List<Models.Submission>();
                try
                {
                    HttpResponseMessage response = client.GetAsync(SolutionsController + GET + "Unevaluated").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        result = response.Content.ReadAsAsync<List<Models.Submission>>().Result;
                    }
                }
                catch (Exception)
                {
                }
                return result;
            }

            public static IEnumerable<Models.Submission> GetUnevaluated(int idContest)
            {
                List<Models.Submission> result = new List<Models.Submission>();
                try
                {
                    HttpResponseMessage response = client.GetAsync(SolutionsController + GET + "Unevaluated?idContest=" + idContest).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        result = response.Content.ReadAsAsync<List<Models.Submission>>().Result;
                    }
                }
                catch (Exception)
                {
                }
                return result;
            }

            public static Models.Submission Get(int id)
            {
                Models.Submission result = null;
                try
                {
                    HttpResponseMessage response = client.GetAsync(SolutionsController + GET + "?id=" + id).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        result = response.Content.ReadAsAsync<Models.Submission>().Result;
                    }
                }
                catch (Exception)
                {
                }
                return result;
            }

            public static byte[] GetSolutionFile(int idSolution)
            {
                try
                {
                    HttpResponseMessage response = client.GetAsync(SolutionsController + GET + "SolutionFile?idSolution=" + idSolution).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        MemoryStream data = new MemoryStream();
                        response.Content.CopyToAsync(data).Wait();
                        return data.ToArray();
                    }
                }
                catch (Exception)
                {
                }
                return null;
            }
        }
        #endregion

        #region Tests
        public class Tests
        {
            public static IEnumerable<Models.Test> Get()
            {
                List<Models.Test> result = new List<Models.Test>();
                try
                {
                    HttpResponseMessage response = client.GetAsync(TestsController + GET).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        result = response.Content.ReadAsAsync<List<Models.Test>>().Result;
                    }
                }
                catch (Exception)
                {
                }
                return result;
            }

            public static IEnumerable<Models.Test> GetByProblemId(int idProblem)
            {
                List<Models.Test> result = new List<Models.Test>();
                try
                {
                    HttpResponseMessage response = client.GetAsync(TestsController + GET + "ByProblemId?idProblem=" + idProblem).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        result = response.Content.ReadAsAsync<List<Models.Test>>().Result;
                    }
                }
                catch (Exception)
                {
                }
                return result;
            }

            public static Models.Test Get(int id)
            {
                Models.Test result = null;
                try
                {
                    HttpResponseMessage response = client.GetAsync(TestsController + GET + "?id=" + id).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        result = response.Content.ReadAsAsync<Models.Test>().Result;
                    }
                }
                catch (Exception)
                {
                }
                return result;
            }

            public static byte[] GetTestFile(int idTest)
            {
                try
                {
                    HttpResponseMessage response = client.GetAsync(TestsController + GET + "TestFile?idTest=" + idTest).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        MemoryStream data = new MemoryStream();
                        response.Content.CopyToAsync(data).Wait();
                        return data.ToArray();
                    }
                }
                catch (Exception)
                {
                }
                return null;
            }

            public static byte[] GetTestOKFile(int idTest)
            {
                try
                {
                    HttpResponseMessage response = client.GetAsync(TestsController + GET + "TestOKFile?idTest=" + idTest).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        MemoryStream data = new MemoryStream();
                        response.Content.CopyToAsync(data).Wait();
                        return data.ToArray();
                    }
                }
                catch (Exception)
                {
                }
                return null;
            }
        }
        #endregion        
    }
}
