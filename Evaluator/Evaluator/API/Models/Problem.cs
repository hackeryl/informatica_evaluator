﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evaluator.API.Models
{
    public class Problem
    {
        public int ID { get; set; }
        public int Id_Contest { get; set; }
        public int Id_Admin { get; set; }
        public string Name { get; set; }
        public int TimeLimit { get; set; }
        public int MemoryLimit { get; set; }
        public string Body { get; set; }
        public string Input { get; set; }
        public string Output { get; set; }
        public string Restrictions { get; set; }
        public string Examples { get; set; }
    }
}
