﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evaluator.API.Models
{
    public class Submission
    {
        public int ID { get; set; }
        public int ID_Problem { get; set; }
        public int ID_Contestant { get; set; }
        public string FileName { get; set; }
        public System.DateTime AcceptedTime { get; set; }
        public int Evaluated { get; set; }
    }
}
