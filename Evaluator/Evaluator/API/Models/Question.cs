﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evaluator.API.Models
{
    public class Question
    {
        public int ID { get; set; }
        public int ID_Contestant { get; set; }
        public Nullable<int> ID_Admin { get; set; }
        public string Question1 { get; set; }
        public string Answer { get; set; }
        public bool Answered { get; set; }
    }
}
