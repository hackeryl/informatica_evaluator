﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evaluator.API.Models
{
    public class SubmissionResult
    {
        public int ID { get; set; }
        public int ID_Submission { get; set; }
        public int ID_Test { get; set; }
        public int ID_Status { get; set; }
        public int Score { get; set; }
        public decimal Time { get; set; }
        public decimal Memory { get; set; }
        public System.DateTime EvaluationTime { get; set; }
    }
}
