﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evaluator.API.Models
{
    public class Test
    {
        public int ID { get; set; }
        public int ID_Problem { get; set; }
        public string Name { get; set; }
        public string InputLocation { get; set; }
        public string OKLocation { get; set; }
        public int Points { get; set; }
    }
}
