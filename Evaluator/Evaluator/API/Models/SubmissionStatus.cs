﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evaluator.API.Models
{
    public enum SubmissionStatus : int
    {
        Submitted = 0,
        InQueue = 1,
        Evaluated = 2
    }
}
