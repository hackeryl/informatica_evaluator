﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Evaluator.API
{
    public class CodEvalApiClient
    {
        #region Constants
        public static readonly string GET = "Get";
        public static readonly string POST = "Post";
        public static readonly string POSTAsync = "PostAsync";
        public static readonly string DELETE = "DELETE";        
        #endregion

        private HttpClient client;
        private string apiKey;
        public Contests Contests { get; private set; }
        public Contestants Contestants { get; private set; }
        public Problems Problems { get; private set; }
        public Statuses Statuses { get; private set; }
        public SubmissionsResults SubmissionsResults { get; private set; }
        public Submissions Submissions { get; private set; }
        public Tests Tests { get; private set; }

        public CodEvalApiClient(string apiUrl, string apiKey)
        {
            client = new HttpClient();
            client.BaseAddress = new Uri(apiUrl);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/octet-stream"));
            this.apiKey = apiKey;
            this.Contests = new Contests(client, apiKey);
            this.Contestants = new Contestants(client, apiKey);
            this.Problems = new Problems(client, apiKey);
            this.Statuses = new Statuses(client, apiKey);
            this.SubmissionsResults = new SubmissionsResults(client, apiKey);
            this.Submissions = new Submissions(client, apiKey);
            this.Tests = new Tests(client, apiKey);
        }                
    }

    #region Contests
    public class Contests
    {
        private static readonly string ContestsController = "Contests/";
        
        private HttpClient client;
        private string apiKey;

        public Contests(HttpClient client, string apiKey)
        {
            this.client = client;
            this.apiKey = apiKey;
        }

        public IEnumerable<Models.Contest> Get()
        {
            List<Models.Contest> result = new List<Models.Contest>();
            try
            {
                HttpResponseMessage response = client.GetAsync(ContestsController + CodEvalApiClient.GET + "?key=" + apiKey).Result;
                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<List<Models.Contest>>().Result;
                }
            }
            catch (Exception)
            {
            }
            return result;
        }

        public Models.Contest Get(int id)
        {
            Models.Contest result = null;
            try
            {
                HttpResponseMessage response = client.GetAsync(ContestsController + CodEvalApiClient.GET + "?key=" + apiKey + "&id=" + id).Result;
                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<Models.Contest>().Result;
                }
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
    #endregion

    #region Contestants
    public class Contestants
    {
        private static readonly string ContestantsController = "Contestants/";
        
        private HttpClient client;
        private string apiKey;

        public Contestants(HttpClient client, string apiKey)
        {
            this.client = client;
            this.apiKey = apiKey;
        }

        public IEnumerable<Models.Contestant> Get()
        {
            List<Models.Contestant> result = new List<Models.Contestant>();
            try
            {
                HttpResponseMessage response = client.GetAsync(ContestantsController + CodEvalApiClient.GET + "?key=" + apiKey).Result;
                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<List<Models.Contestant>>().Result;
                }
            }
            catch (Exception)
            {
            }
            return result;
        }

        public Models.Contestant Get(int id)
        {
            Models.Contestant result = null;
            try
            {
                HttpResponseMessage response = client.GetAsync(ContestantsController + CodEvalApiClient.GET + "?key=" + apiKey + "&id=" + id).Result;
                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<Models.Contestant>().Result;
                }
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
    #endregion

    #region Problems
    public class Problems
    {
        private static readonly string ProblemsController = "Problems/";
        
        private HttpClient client;
        private string apiKey;

        public Problems(HttpClient client, string apiKey)
        {
            this.client = client;
            this.apiKey = apiKey;
        }

        public IEnumerable<Models.Problem> Get()
        {
            List<Models.Problem> result = new List<Models.Problem>();
            try
            {
                HttpResponseMessage response = client.GetAsync(ProblemsController + CodEvalApiClient.GET + "?key=" + apiKey).Result;
                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<List<Models.Problem>>().Result;
                }
            }
            catch (Exception)
            {
            }
            return result;
        }

        public IEnumerable<Models.Problem> GetByContestId(int idContest)
        {
            List<Models.Problem> result = new List<Models.Problem>();
            try
            {
                HttpResponseMessage response = client.GetAsync(ProblemsController + "GetByContestId?idContest=" + idContest + "&key=" + apiKey).Result;
                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<List<Models.Problem>>().Result;
                }
            }
            catch (Exception)
            {
            }
            return result;
        }

        public Models.Problem Get(int id)
        {
            Models.Problem result = null;
            try
            {
                HttpResponseMessage response = client.GetAsync(ProblemsController + CodEvalApiClient.GET + "?key=" + apiKey + "&id=" + id).Result;
                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<Models.Problem>().Result;
                }
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
    #endregion

    #region Statuses
    public class Statuses
    {        
        private static readonly string StatusesController = "Statuses/";
        
        private HttpClient client;
        private string apiKey;

        public Statuses(HttpClient client, string apiKey)
        {
            this.client = client;
            this.apiKey = apiKey;
        }

        public IEnumerable<Models.Status> Get()
        {
            List<Models.Status> result = new List<Models.Status>();
            try
            {
                HttpResponseMessage response = client.GetAsync(StatusesController + CodEvalApiClient.GET + "?key=" + apiKey).Result;
                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<List<Models.Status>>().Result;
                }
            }
            catch (Exception)
            {
            }
            return result;
        }

        public Models.Status Get(int id)
        {
            Models.Status result = null;
            try
            {
                HttpResponseMessage response = client.GetAsync(StatusesController + CodEvalApiClient.GET + "?key=" + apiKey + "&id=" + id).Result;
                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<Models.Status>().Result;
                }
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
    #endregion

    #region SubmissionsResults
    public class SubmissionsResults
    {
        private static readonly string SubmissionsResultsController = "SubmissionsResults/";
        
        private HttpClient client;
        private string apiKey;

        public SubmissionsResults(HttpClient client, string apiKey)
        {
            this.client = client;
            this.apiKey = apiKey;
        }

        public IEnumerable<Models.SubmissionResult> Get()
        {
            List<Models.SubmissionResult> result = new List<Models.SubmissionResult>();
            try
            {
                HttpResponseMessage response = client.GetAsync(SubmissionsResultsController + CodEvalApiClient.GET + "?key=" + apiKey).Result;
                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<List<Models.SubmissionResult>>().Result;
                }
            }
            catch (Exception)
            {
            }
            return result;
        }

        public Models.SubmissionResult Get(int id)
        {
            Models.SubmissionResult result = null;
            try
            {
                HttpResponseMessage response = client.GetAsync(SubmissionsResultsController + CodEvalApiClient.GET + "?key=" + apiKey + "&id=" + id).Result;
                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<Models.SubmissionResult>().Result;
                }
            }
            catch (Exception)
            {
            }
            return result;
        }

        public bool Post(IEnumerable<Models.SubmissionResult> results)
        {
            try
            {
                HttpResponseMessage response = client.PostAsJsonAsync(SubmissionsResultsController + CodEvalApiClient.POST + "?key=" + apiKey, results).Result;
                return response.IsSuccessStatusCode;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool PostAsync(IEnumerable<Models.SubmissionResult> results)
        {
            try
            {
                HttpResponseMessage response = client.PostAsJsonAsync(SubmissionsResultsController + CodEvalApiClient.POSTAsync + "?key=" + apiKey, results).Result;
                return response.IsSuccessStatusCode;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                HttpResponseMessage response = client.DeleteAsync(SubmissionsResultsController + CodEvalApiClient.DELETE + "?key=" + apiKey + "&id=" + id).Result;
                return response.IsSuccessStatusCode;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
    #endregion

    #region Submissions
    public class Submissions
    {
        private static readonly string SubmissionsController = "Submissions/";
        
        private HttpClient client;
        private string apiKey;

        public Submissions(HttpClient client, string apiKey)
        {
            this.client = client;
            this.apiKey = apiKey;
        }

        public IEnumerable<Models.Submission> Get()
        {
            List<Models.Submission> result = new List<Models.Submission>();
            try
            {
                HttpResponseMessage response = client.GetAsync(SubmissionsController + CodEvalApiClient.GET + "?key=" + apiKey).Result;
                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<List<Models.Submission>>().Result;
                }
            }
            catch (Exception)
            {
            }
            return result;
        }

        public IEnumerable<Models.Submission> GetByStatus(Models.SubmissionStatus status, Models.SubmissionStatus? newStatus)
        {
            List<Models.Submission> result = new List<Models.Submission>();
            try
            {
                HttpResponseMessage response = client.GetAsync(SubmissionsController + CodEvalApiClient.GET + "ByStatus?status=" + (int)status + (newStatus.HasValue ? "&newStatus=" + ((int)newStatus.Value).ToString() : String.Empty) + "&key=" + apiKey).Result;
                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<List<Models.Submission>>().Result;
                }
            }
            catch (Exception)
            {
            }
            return result;
        }

        public IEnumerable<Models.Submission> GetByContest_Status(int idContest, Models.SubmissionStatus currentStatus, Models.SubmissionStatus? newStatus)
        {
            List<Models.Submission> result = new List<Models.Submission>();
            try
            {
                HttpResponseMessage response = client.GetAsync(SubmissionsController + CodEvalApiClient.GET + "ByContest_Status?idContest=" + idContest + "&currentStatus=" + (int)currentStatus + "&newStatus=" + (newStatus.HasValue ? ((int)newStatus.Value).ToString() : String.Empty) + "&key=" + apiKey).Result;
                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<List<Models.Submission>>().Result;
                }
            }
            catch (Exception)
            {
            }
            return result;
        }

        public bool SetStatus(int id, Models.SubmissionStatus status)
        {
            try
            {
                HttpResponseMessage response = client.PostAsJsonAsync(SubmissionsController + "SetStatus?id=" + id + "&key=" + apiKey, status).Result;
                return response.IsSuccessStatusCode;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public Models.Submission Get(int id)
        {
            Models.Submission result = null;
            try
            {
                HttpResponseMessage response = client.GetAsync(SubmissionsController + CodEvalApiClient.GET + "?key=" + apiKey + "&id=" + id).Result;
                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<Models.Submission>().Result;
                }
            }
            catch (Exception)
            {
            }
            return result;
        }

        public byte[] GetSubmissionFile(int id)
        {
            try
            {
                HttpResponseMessage response = client.GetAsync(SubmissionsController + CodEvalApiClient.GET + "SubmissionFile?id=" + id + "&key=" + apiKey).Result;
                if (response.IsSuccessStatusCode)
                {
                    MemoryStream data = new MemoryStream();
                    response.Content.CopyToAsync(data).Wait();
                    return data.ToArray();
                }
            }
            catch (Exception)
            {
            }
            return null;
        }
    }
    #endregion

    #region Tests
    public class Tests
    {
        private static readonly string TestsController = "Tests/";

        private HttpClient client;
        private string apiKey;

        public Tests(HttpClient client, string apiKey)
        {
            this.client = client;
            this.apiKey = apiKey;
        }

        public IEnumerable<Models.Test> Get()
        {
            List<Models.Test> result = new List<Models.Test>();
            try
            {
                HttpResponseMessage response = client.GetAsync(TestsController + CodEvalApiClient.GET + "?key=" + apiKey).Result;
                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<List<Models.Test>>().Result;
                }
            }
            catch (Exception)
            {
            }
            return result;
        }

        public IEnumerable<Models.Test> GetByProblemId(int idProblem)
        {
            List<Models.Test> result = new List<Models.Test>();
            try
            {
                HttpResponseMessage response = client.GetAsync(TestsController + CodEvalApiClient.GET + "ByProblemId?idProblem=" + idProblem + "&key=" + apiKey).Result;
                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<List<Models.Test>>().Result;
                }
            }
            catch (Exception)
            {
            }
            return result;
        }

        public Models.Test Get(int id)
        {
            Models.Test result = null;
            try
            {
                HttpResponseMessage response = client.GetAsync(TestsController + CodEvalApiClient.GET + "?key=" + apiKey + "&id=" + id).Result;
                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<Models.Test>().Result;
                }
            }
            catch (Exception)
            {
            }
            return result;
        }

        public byte[] GetTestInputFile(int idTest)
        {
            try
            {
                HttpResponseMessage response = client.GetAsync(TestsController + CodEvalApiClient.GET + "InputFile?id=" + idTest + "&key=" + apiKey).Result;
                if (response.IsSuccessStatusCode)
                {
                    MemoryStream data = new MemoryStream();
                    response.Content.CopyToAsync(data).Wait();
                    return data.ToArray();
                }
            }
            catch (Exception)
            {
            }
            return null;
        }

        public byte[] GetTestOKFile(int idTest)
        {
            try
            {
                HttpResponseMessage response = client.GetAsync(TestsController + CodEvalApiClient.GET + "OKFile?id=" + idTest + "&key=" + apiKey).Result;
                if (response.IsSuccessStatusCode)
                {
                    MemoryStream data = new MemoryStream();
                    response.Content.CopyToAsync(data).Wait();
                    return data.ToArray();
                }
            }
            catch (Exception)
            {
            }
            return null;
        }
    }
    #endregion        
}
