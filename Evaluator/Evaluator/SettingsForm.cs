﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Evaluator
{
    public partial class SettingsForm : Form
    {
        private Microsoft.Win32.RegistryKey key;
        public SettingsForm()
        {
            InitializeComponent();
            key = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(Constants.Evaluator);
        }

        private void SettingsForm_Load(object sender, EventArgs e)
        {
            tbGPP.Text = key.GetValue(Constants.GPPCompiler) != null ? key.GetValue(Constants.GPPCompiler).ToString() : String.Empty;
            tbPascal.Text = key.GetValue(Constants.PascalCompiler) != null ? key.GetValue(Constants.PascalCompiler).ToString() : String.Empty;
            tbEvaluationPath.Text = key.GetValue(Constants.EvaluationPath) != null ? key.GetValue(Constants.EvaluationPath).ToString() : String.Empty;
            tbApiUrl.Text = key.GetValue(Constants.ApiUrl) != null ? key.GetValue(Constants.ApiUrl).ToString() : String.Empty;
            tbApiKey.Text = key.GetValue(Constants.ApiKey) != null ? key.GetValue(Constants.ApiKey).ToString() : String.Empty;
            tbSandboxStart.Text = key.GetValue(Constants.SandboxStartPath) != null ? key.GetValue(Constants.SandboxStartPath).ToString() : String.Empty;
            tbSandboxTemp.Text = key.GetValue(Constants.SandboxTempPath) != null ? key.GetValue(Constants.SandboxTempPath).ToString() : String.Empty;
        }

        private void SettingsForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            key.Close();
        }

        private void btnSelectPascal_Click(object sender, EventArgs e)
        {
            OpenFileDialog opd = new OpenFileDialog();
            opd.Multiselect = false;
            opd.Filter = "All Files|*.exe";
            if (opd.ShowDialog() == DialogResult.OK)
            {
                key.SetValue(Constants.PascalCompiler, opd.FileName);
                tbPascal.Text = opd.FileName;
            }            
        }

        private void btnSelectGPP_Click(object sender, EventArgs e)
        {
            OpenFileDialog opd = new OpenFileDialog();
            opd.Multiselect = false;
            opd.Filter = "All Files|*.exe";
            if (opd.ShowDialog() == DialogResult.OK)
            {
                key.SetValue(Constants.GPPCompiler, opd.FileName);
                tbGPP.Text = opd.FileName;
            }   
        }        

        private void btnSelectEvaluationPath_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.ShowNewFolderButton = false;
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                key.SetValue(Constants.EvaluationPath, fbd.SelectedPath);
                tbEvaluationPath.Text = fbd.SelectedPath;
            }
        }

        private void btnApiUrl_Click(object sender, EventArgs e)
        {
            key.SetValue(Constants.ApiUrl, tbApiUrl.Text);
        }

        private void btnApiKey_Click(object sender, EventArgs e)
        {
            key.SetValue(Constants.ApiKey, tbApiKey.Text);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Saved!");
            this.Close();
        }

        private void btnSandboxStart_Click(object sender, EventArgs e)
        {
            OpenFileDialog opd = new OpenFileDialog();
            opd.Multiselect = false;
            opd.Filter = "All Files|*.exe";
            if (opd.ShowDialog() == DialogResult.OK)
            {
                key.SetValue(Constants.SandboxStartPath, opd.FileName);
                tbSandboxStart.Text = opd.FileName;
            }   
        }

        private void btnSandboxTemp_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.ShowNewFolderButton = false;
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                key.SetValue(Constants.SandboxTempPath, fbd.SelectedPath);
                tbSandboxTemp.Text = fbd.SelectedPath;
            }
        }        
    }
}
