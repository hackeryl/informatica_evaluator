﻿namespace Evaluator
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblContestant = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.tbPascal = new DevExpress.XtraEditors.TextEdit();
            this.tbGPP = new DevExpress.XtraEditors.TextEdit();
            this.tbEvaluationPath = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.btnResults = new DevExpress.XtraEditors.SimpleButton();
            this.tbApiUrl = new DevExpress.XtraEditors.TextEdit();
            this.lblResults = new DevExpress.XtraEditors.LabelControl();
            this.btnApiKey = new DevExpress.XtraEditors.SimpleButton();
            this.tbApiKey = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.btnSandboxStart = new DevExpress.XtraEditors.SimpleButton();
            this.tbSandboxStart = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.btnSandboxTemp = new DevExpress.XtraEditors.SimpleButton();
            this.tbSandboxTemp = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.tbPascal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbGPP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbEvaluationPath.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbApiUrl.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbApiKey.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSandboxStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSandboxTemp.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lblContestant
            // 
            this.lblContestant.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.lblContestant.Location = new System.Drawing.Point(12, 12);
            this.lblContestant.Name = "lblContestant";
            this.lblContestant.Size = new System.Drawing.Size(219, 24);
            this.lblContestant.TabIndex = 3;
            this.lblContestant.Text = "Pascal Compiler Path:";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.labelControl1.Location = new System.Drawing.Point(39, 42);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(192, 24);
            this.labelControl1.TabIndex = 4;
            this.labelControl1.Text = "Gcc Compiler Path:";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.labelControl2.Location = new System.Drawing.Point(66, 72);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(165, 24);
            this.labelControl2.TabIndex = 5;
            this.labelControl2.Text = "Evaluation Path:";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(12, 220);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(476, 32);
            this.btnSave.TabIndex = 7;
            this.btnSave.Text = "Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // tbPascal
            // 
            this.tbPascal.Location = new System.Drawing.Point(238, 16);
            this.tbPascal.Name = "tbPascal";
            this.tbPascal.Properties.ReadOnly = true;
            this.tbPascal.Size = new System.Drawing.Size(176, 20);
            this.tbPascal.TabIndex = 8;
            // 
            // tbGPP
            // 
            this.tbGPP.Location = new System.Drawing.Point(238, 47);
            this.tbGPP.Name = "tbGPP";
            this.tbGPP.Properties.ReadOnly = true;
            this.tbGPP.Size = new System.Drawing.Size(176, 20);
            this.tbGPP.TabIndex = 9;
            // 
            // tbEvaluationPath
            // 
            this.tbEvaluationPath.Location = new System.Drawing.Point(238, 76);
            this.tbEvaluationPath.Name = "tbEvaluationPath";
            this.tbEvaluationPath.Properties.ReadOnly = true;
            this.tbEvaluationPath.Size = new System.Drawing.Size(176, 20);
            this.tbEvaluationPath.TabIndex = 10;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(420, 14);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 12;
            this.simpleButton1.Text = "Select";
            this.simpleButton1.Click += new System.EventHandler(this.btnSelectPascal_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(420, 45);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 13;
            this.simpleButton2.Text = "Select";
            this.simpleButton2.Click += new System.EventHandler(this.btnSelectGPP_Click);
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(420, 74);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(75, 23);
            this.simpleButton3.TabIndex = 14;
            this.simpleButton3.Text = "Select";
            this.simpleButton3.Click += new System.EventHandler(this.btnSelectEvaluationPath_Click);
            // 
            // btnResults
            // 
            this.btnResults.Location = new System.Drawing.Point(420, 103);
            this.btnResults.Name = "btnResults";
            this.btnResults.Size = new System.Drawing.Size(75, 23);
            this.btnResults.TabIndex = 17;
            this.btnResults.Text = "Select";
            this.btnResults.Click += new System.EventHandler(this.btnApiUrl_Click);
            // 
            // tbApiUrl
            // 
            this.tbApiUrl.AllowDrop = true;
            this.tbApiUrl.Location = new System.Drawing.Point(238, 105);
            this.tbApiUrl.Name = "tbApiUrl";
            this.tbApiUrl.Size = new System.Drawing.Size(176, 20);
            this.tbApiUrl.TabIndex = 16;
            // 
            // lblResults
            // 
            this.lblResults.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.lblResults.Location = new System.Drawing.Point(151, 103);
            this.lblResults.Name = "lblResults";
            this.lblResults.Size = new System.Drawing.Size(80, 24);
            this.lblResults.TabIndex = 15;
            this.lblResults.Text = "API Url:";
            // 
            // btnApiKey
            // 
            this.btnApiKey.Location = new System.Drawing.Point(420, 132);
            this.btnApiKey.Name = "btnApiKey";
            this.btnApiKey.Size = new System.Drawing.Size(75, 23);
            this.btnApiKey.TabIndex = 20;
            this.btnApiKey.Text = "Select";
            this.btnApiKey.Click += new System.EventHandler(this.btnApiKey_Click);
            // 
            // tbApiKey
            // 
            this.tbApiKey.AllowDrop = true;
            this.tbApiKey.Location = new System.Drawing.Point(238, 134);
            this.tbApiKey.Name = "tbApiKey";
            this.tbApiKey.Size = new System.Drawing.Size(176, 20);
            this.tbApiKey.TabIndex = 19;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.labelControl3.Location = new System.Drawing.Point(143, 129);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(88, 24);
            this.labelControl3.TabIndex = 18;
            this.labelControl3.Text = "API Key:";
            // 
            // btnSandboxStart
            // 
            this.btnSandboxStart.Location = new System.Drawing.Point(420, 161);
            this.btnSandboxStart.Name = "btnSandboxStart";
            this.btnSandboxStart.Size = new System.Drawing.Size(75, 23);
            this.btnSandboxStart.TabIndex = 23;
            this.btnSandboxStart.Text = "Select";
            this.btnSandboxStart.Click += new System.EventHandler(this.btnSandboxStart_Click);
            // 
            // tbSandboxStart
            // 
            this.tbSandboxStart.Location = new System.Drawing.Point(238, 163);
            this.tbSandboxStart.Name = "tbSandboxStart";
            this.tbSandboxStart.Properties.ReadOnly = true;
            this.tbSandboxStart.Size = new System.Drawing.Size(176, 20);
            this.tbSandboxStart.TabIndex = 22;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.labelControl4.Location = new System.Drawing.Point(80, 159);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(151, 24);
            this.labelControl4.TabIndex = 21;
            this.labelControl4.Text = "Sandbox Start:";
            // 
            // btnSandboxTemp
            // 
            this.btnSandboxTemp.Location = new System.Drawing.Point(420, 191);
            this.btnSandboxTemp.Name = "btnSandboxTemp";
            this.btnSandboxTemp.Size = new System.Drawing.Size(75, 23);
            this.btnSandboxTemp.TabIndex = 26;
            this.btnSandboxTemp.Text = "Select";
            this.btnSandboxTemp.Click += new System.EventHandler(this.btnSandboxTemp_Click);
            // 
            // tbSandboxTemp
            // 
            this.tbSandboxTemp.Location = new System.Drawing.Point(238, 193);
            this.tbSandboxTemp.Name = "tbSandboxTemp";
            this.tbSandboxTemp.Properties.ReadOnly = true;
            this.tbSandboxTemp.Size = new System.Drawing.Size(176, 20);
            this.tbSandboxTemp.TabIndex = 25;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.labelControl5.Location = new System.Drawing.Point(74, 189);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(157, 24);
            this.labelControl5.TabIndex = 24;
            this.labelControl5.Text = "Sandbox Temp:";
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(500, 260);
            this.Controls.Add(this.btnSandboxTemp);
            this.Controls.Add(this.tbSandboxTemp);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.btnSandboxStart);
            this.Controls.Add(this.tbSandboxStart);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.btnApiKey);
            this.Controls.Add(this.tbApiKey);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.btnResults);
            this.Controls.Add(this.tbApiUrl);
            this.Controls.Add(this.lblResults);
            this.Controls.Add(this.simpleButton3);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.tbEvaluationPath);
            this.Controls.Add(this.tbGPP);
            this.Controls.Add(this.tbPascal);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.lblContestant);
            this.Name = "SettingsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "SettingsForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.SettingsForm_FormClosed);
            this.Load += new System.EventHandler(this.SettingsForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tbPascal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbGPP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbEvaluationPath.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbApiUrl.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbApiKey.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSandboxStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSandboxTemp.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl lblContestant;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.TextEdit tbPascal;
        private DevExpress.XtraEditors.TextEdit tbGPP;
        private DevExpress.XtraEditors.TextEdit tbEvaluationPath;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton btnResults;
        private DevExpress.XtraEditors.TextEdit tbApiUrl;
        private DevExpress.XtraEditors.LabelControl lblResults;
        private DevExpress.XtraEditors.SimpleButton btnApiKey;
        private DevExpress.XtraEditors.TextEdit tbApiKey;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.SimpleButton btnSandboxStart;
        private DevExpress.XtraEditors.TextEdit tbSandboxStart;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.SimpleButton btnSandboxTemp;
        private DevExpress.XtraEditors.TextEdit tbSandboxTemp;
        private DevExpress.XtraEditors.LabelControl labelControl5;
    }
}