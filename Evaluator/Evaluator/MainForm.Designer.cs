﻿namespace Evaluator
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.menuMain = new System.Windows.Forms.MenuStrip();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.compilerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.howToUseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contactToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lblContestant = new DevExpress.XtraEditors.LabelControl();
            this.lblContest = new DevExpress.XtraEditors.LabelControl();
            this.lblContestantName = new DevExpress.XtraEditors.LabelControl();
            this.lblContestName = new DevExpress.XtraEditors.LabelControl();
            this.lblProblem = new DevExpress.XtraEditors.LabelControl();
            this.lblProblemValue = new DevExpress.XtraEditors.LabelControl();
            this.pictureBoxSVOInfo = new System.Windows.Forms.PictureBox();
            this.btnStartEval = new DevExpress.XtraEditors.SimpleButton();
            this.bgWEvaluator = new System.ComponentModel.BackgroundWorker();
            this.statusBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridResults = new DevExpress.XtraGrid.GridControl();
            this.gridViewResults = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnContest = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnContestant = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnProblem = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnFileName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnMessage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnScore = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnIsCorrect = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnMemory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.notificationsGrid = new System.Windows.Forms.DataGridView();
            this.messageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.notificationsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.menuMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSVOInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridResults)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewResults)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.notificationsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.notificationsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // menuMain
            // 
            this.menuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuMain.Location = new System.Drawing.Point(0, 0);
            this.menuMain.Name = "menuMain";
            this.menuMain.Size = new System.Drawing.Size(1008, 24);
            this.menuMain.TabIndex = 1;
            this.menuMain.Text = "menuMain";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.compilerToolStripMenuItem});
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.settingsToolStripMenuItem.Text = "Settings";
            // 
            // compilerToolStripMenuItem
            // 
            this.compilerToolStripMenuItem.Name = "compilerToolStripMenuItem";
            this.compilerToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.compilerToolStripMenuItem.Text = "General";
            this.compilerToolStripMenuItem.Click += new System.EventHandler(this.compilerToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.howToUseToolStripMenuItem,
            this.contactToolStripMenuItem});
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // howToUseToolStripMenuItem
            // 
            this.howToUseToolStripMenuItem.Name = "howToUseToolStripMenuItem";
            this.howToUseToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
            this.howToUseToolStripMenuItem.Text = "How to use";
            this.howToUseToolStripMenuItem.Click += new System.EventHandler(this.howToUseToolStripMenuItem_Click);
            // 
            // contactToolStripMenuItem
            // 
            this.contactToolStripMenuItem.Name = "contactToolStripMenuItem";
            this.contactToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
            this.contactToolStripMenuItem.Text = "Contact";
            this.contactToolStripMenuItem.Click += new System.EventHandler(this.contactToolStripMenuItem_Click);
            // 
            // lblContestant
            // 
            this.lblContestant.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.lblContestant.Location = new System.Drawing.Point(19, 56);
            this.lblContestant.Name = "lblContestant";
            this.lblContestant.Size = new System.Drawing.Size(130, 24);
            this.lblContestant.TabIndex = 2;
            this.lblContestant.Text = "Concurentul:";
            this.lblContestant.Visible = false;
            // 
            // lblContest
            // 
            this.lblContest.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.lblContest.Location = new System.Drawing.Point(33, 27);
            this.lblContest.Name = "lblContest";
            this.lblContest.Size = new System.Drawing.Size(116, 24);
            this.lblContest.TabIndex = 3;
            this.lblContest.Text = "Competitia:";
            this.lblContest.Visible = false;
            // 
            // lblContestantName
            // 
            this.lblContestantName.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.lblContestantName.Location = new System.Drawing.Point(166, 56);
            this.lblContestantName.Name = "lblContestantName";
            this.lblContestantName.Size = new System.Drawing.Size(58, 24);
            this.lblContestantName.TabIndex = 4;
            this.lblContestantName.Text = "Name";
            this.lblContestantName.Visible = false;
            // 
            // lblContestName
            // 
            this.lblContestName.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.lblContestName.Location = new System.Drawing.Point(167, 27);
            this.lblContestName.Name = "lblContestName";
            this.lblContestName.Size = new System.Drawing.Size(120, 24);
            this.lblContestName.TabIndex = 5;
            this.lblContestName.Text = "Informatica";
            this.lblContestName.Visible = false;
            // 
            // lblProblem
            // 
            this.lblProblem.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.lblProblem.Location = new System.Drawing.Point(46, 86);
            this.lblProblem.Name = "lblProblem";
            this.lblProblem.Size = new System.Drawing.Size(103, 24);
            this.lblProblem.TabIndex = 6;
            this.lblProblem.Text = "Problema:";
            this.lblProblem.Visible = false;
            // 
            // lblProblemValue
            // 
            this.lblProblemValue.Appearance.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold);
            this.lblProblemValue.Location = new System.Drawing.Point(166, 88);
            this.lblProblemValue.Name = "lblProblemValue";
            this.lblProblemValue.Size = new System.Drawing.Size(84, 24);
            this.lblProblemValue.TabIndex = 7;
            this.lblProblemValue.Text = "Problem";
            this.lblProblemValue.Visible = false;
            // 
            // pictureBoxSVOInfo
            // 
            this.pictureBoxSVOInfo.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxSVOInfo.Image")));
            this.pictureBoxSVOInfo.Location = new System.Drawing.Point(46, 27);
            this.pictureBoxSVOInfo.Name = "pictureBoxSVOInfo";
            this.pictureBoxSVOInfo.Size = new System.Drawing.Size(128, 130);
            this.pictureBoxSVOInfo.TabIndex = 9;
            this.pictureBoxSVOInfo.TabStop = false;
            // 
            // btnStartEval
            // 
            this.btnStartEval.Appearance.Font = new System.Drawing.Font("Tahoma", 20F, System.Drawing.FontStyle.Bold);
            this.btnStartEval.Appearance.Options.UseFont = true;
            this.btnStartEval.Location = new System.Drawing.Point(4, 176);
            this.btnStartEval.Name = "btnStartEval";
            this.btnStartEval.Size = new System.Drawing.Size(194, 83);
            this.btnStartEval.TabIndex = 10;
            this.btnStartEval.Text = "Start";
            this.btnStartEval.Click += new System.EventHandler(this.btnStartEval_Click);
            // 
            // bgWEvaluator
            // 
            this.bgWEvaluator.WorkerReportsProgress = true;
            this.bgWEvaluator.WorkerSupportsCancellation = true;
            this.bgWEvaluator.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWEvaluator_DoWork);
            this.bgWEvaluator.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgWEvaluator_ProgressChanged);
            this.bgWEvaluator.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWEvaluator_RunWorkerCompleted);
            // 
            // statusBindingSource
            // 
            this.statusBindingSource.DataSource = typeof(Evaluator.Status);
            // 
            // gridResults
            // 
            this.gridResults.DataSource = this.statusBindingSource;
            this.gridResults.Location = new System.Drawing.Point(4, 265);
            this.gridResults.MainView = this.gridViewResults;
            this.gridResults.Name = "gridResults";
            this.gridResults.Size = new System.Drawing.Size(1000, 284);
            this.gridResults.TabIndex = 11;
            this.gridResults.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewResults});
            // 
            // gridViewResults
            // 
            this.gridViewResults.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnContest,
            this.gridColumnContestant,
            this.gridColumnProblem,
            this.gridColumnFileName,
            this.gridColumnMessage,
            this.gridColumnTime,
            this.gridColumnScore,
            this.gridColumnIsCorrect,
            this.gridColumnMemory});
            this.gridViewResults.GridControl = this.gridResults;
            this.gridViewResults.Name = "gridViewResults";
            this.gridViewResults.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumnContest
            // 
            this.gridColumnContest.Caption = "Contest";
            this.gridColumnContest.FieldName = "Contest";
            this.gridColumnContest.Name = "gridColumnContest";
            this.gridColumnContest.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.gridColumnContest.Visible = true;
            this.gridColumnContest.VisibleIndex = 0;
            this.gridColumnContest.Width = 105;
            // 
            // gridColumnContestant
            // 
            this.gridColumnContestant.Caption = "Contestant";
            this.gridColumnContestant.FieldName = "Contestant";
            this.gridColumnContestant.Name = "gridColumnContestant";
            this.gridColumnContestant.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.gridColumnContestant.Visible = true;
            this.gridColumnContestant.VisibleIndex = 1;
            this.gridColumnContestant.Width = 108;
            // 
            // gridColumnProblem
            // 
            this.gridColumnProblem.Caption = "Problem";
            this.gridColumnProblem.FieldName = "Problem";
            this.gridColumnProblem.Name = "gridColumnProblem";
            this.gridColumnProblem.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.gridColumnProblem.Visible = true;
            this.gridColumnProblem.VisibleIndex = 2;
            this.gridColumnProblem.Width = 105;
            // 
            // gridColumnFileName
            // 
            this.gridColumnFileName.Caption = "Input File";
            this.gridColumnFileName.FieldName = "InputFileName";
            this.gridColumnFileName.Name = "gridColumnFileName";
            this.gridColumnFileName.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.gridColumnFileName.Visible = true;
            this.gridColumnFileName.VisibleIndex = 3;
            this.gridColumnFileName.Width = 105;
            // 
            // gridColumnMessage
            // 
            this.gridColumnMessage.Caption = "Message";
            this.gridColumnMessage.FieldName = "Message";
            this.gridColumnMessage.Name = "gridColumnMessage";
            this.gridColumnMessage.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.gridColumnMessage.Visible = true;
            this.gridColumnMessage.VisibleIndex = 4;
            this.gridColumnMessage.Width = 143;
            // 
            // gridColumnTime
            // 
            this.gridColumnTime.Caption = "Time";
            this.gridColumnTime.FieldName = "Time";
            this.gridColumnTime.Name = "gridColumnTime";
            this.gridColumnTime.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.gridColumnTime.Visible = true;
            this.gridColumnTime.VisibleIndex = 5;
            this.gridColumnTime.Width = 52;
            // 
            // gridColumnScore
            // 
            this.gridColumnScore.Caption = "Score";
            this.gridColumnScore.FieldName = "Score";
            this.gridColumnScore.Name = "gridColumnScore";
            this.gridColumnScore.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            this.gridColumnScore.Visible = true;
            this.gridColumnScore.VisibleIndex = 6;
            this.gridColumnScore.Width = 52;
            // 
            // gridColumnIsCorrect
            // 
            this.gridColumnIsCorrect.Caption = "IsCorrect";
            this.gridColumnIsCorrect.FieldName = "IsCorrect";
            this.gridColumnIsCorrect.Name = "gridColumnIsCorrect";
            this.gridColumnIsCorrect.UnboundType = DevExpress.Data.UnboundColumnType.Boolean;
            this.gridColumnIsCorrect.Visible = true;
            this.gridColumnIsCorrect.VisibleIndex = 7;
            this.gridColumnIsCorrect.Width = 50;
            // 
            // gridColumnMemory
            // 
            this.gridColumnMemory.Caption = "Memory";
            this.gridColumnMemory.FieldName = "Memory";
            this.gridColumnMemory.Name = "gridColumnMemory";
            this.gridColumnMemory.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.gridColumnMemory.Width = 60;
            // 
            // notificationsGrid
            // 
            this.notificationsGrid.AutoGenerateColumns = false;
            this.notificationsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.notificationsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.messageDataGridViewTextBoxColumn,
            this.Time});
            this.notificationsGrid.DataSource = this.notificationsBindingSource;
            this.notificationsGrid.Location = new System.Drawing.Point(204, 27);
            this.notificationsGrid.Name = "notificationsGrid";
            this.notificationsGrid.Size = new System.Drawing.Size(800, 232);
            this.notificationsGrid.TabIndex = 12;
            // 
            // messageDataGridViewTextBoxColumn
            // 
            this.messageDataGridViewTextBoxColumn.DataPropertyName = "Message";
            this.messageDataGridViewTextBoxColumn.HeaderText = "Notifications";
            this.messageDataGridViewTextBoxColumn.MinimumWidth = 100;
            this.messageDataGridViewTextBoxColumn.Name = "messageDataGridViewTextBoxColumn";
            this.messageDataGridViewTextBoxColumn.ReadOnly = true;
            this.messageDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.messageDataGridViewTextBoxColumn.Width = 600;
            // 
            // Time
            // 
            this.Time.DataPropertyName = "Time";
            dataGridViewCellStyle1.Format = "G";
            dataGridViewCellStyle1.NullValue = null;
            this.Time.DefaultCellStyle = dataGridViewCellStyle1;
            this.Time.HeaderText = "Time";
            this.Time.MinimumWidth = 100;
            this.Time.Name = "Time";
            this.Time.ReadOnly = true;
            this.Time.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Time.Width = 135;
            // 
            // notificationsBindingSource
            // 
            this.notificationsBindingSource.DataSource = typeof(Evaluator.Judge.Notification);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 561);
            this.Controls.Add(this.notificationsGrid);
            this.Controls.Add(this.gridResults);
            this.Controls.Add(this.btnStartEval);
            this.Controls.Add(this.pictureBoxSVOInfo);
            this.Controls.Add(this.lblProblemValue);
            this.Controls.Add(this.lblProblem);
            this.Controls.Add(this.lblContestName);
            this.Controls.Add(this.lblContestantName);
            this.Controls.Add(this.lblContest);
            this.Controls.Add(this.lblContestant);
            this.Controls.Add(this.menuMain);
            this.MainMenuStrip = this.menuMain;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Evaluator";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.menuMain.ResumeLayout(false);
            this.menuMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSVOInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridResults)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewResults)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.notificationsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.notificationsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuMain;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem compilerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem howToUseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contactToolStripMenuItem;
        private DevExpress.XtraEditors.LabelControl lblContestant;
        private DevExpress.XtraEditors.LabelControl lblContest;
        private DevExpress.XtraEditors.LabelControl lblContestantName;
        private DevExpress.XtraEditors.LabelControl lblContestName;
        private DevExpress.XtraEditors.LabelControl lblProblem;
        private DevExpress.XtraEditors.LabelControl lblProblemValue;
        private System.Windows.Forms.PictureBox pictureBoxSVOInfo;
        private DevExpress.XtraEditors.SimpleButton btnStartEval;
        private System.ComponentModel.BackgroundWorker bgWEvaluator;
        private System.Windows.Forms.BindingSource statusBindingSource;
        private DevExpress.XtraGrid.GridControl gridResults;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewResults;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnFileName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnMessage;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnMemory;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnTime;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnScore;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnIsCorrect;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnContest;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnContestant;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnProblem;
        private System.Windows.Forms.DataGridView notificationsGrid;
        private System.Windows.Forms.BindingSource notificationsBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn messageDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Time;
    }
}

