﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evaluator.Judge
{
    public class JudgeNotificationEventArgs : EventArgs
    {
        public Notification Notification { get; private set; }

        public JudgeNotificationEventArgs(string message, DateTime time)
        {
            Notification = new Notification(message, time);
        }
    }
}
