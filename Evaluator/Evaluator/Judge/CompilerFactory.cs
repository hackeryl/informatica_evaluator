﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evaluator.Judge
{
    public class CompilerFactory
    {
        private static Dictionary<string, ICompiler> compilers = new Dictionary<string, ICompiler>();

        public static void RegisterCompiler(string name, ICompiler compiler)
        {
            compilers[name] = compiler;
        }

        public static void RemoveCompiler(string name)
        {
            if (compilers[name] != null)
            {
                compilers.Remove(name);
            }
        }

        public static ICompiler GetCompiler(string name)
        {
            return compilers[name];
        }
    }
}
