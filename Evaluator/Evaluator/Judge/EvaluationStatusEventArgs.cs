﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evaluator.Judge
{
    public class EvaluationResultEventArgs : EventArgs
    {
        public API.Models.Contest Contest { get; private set; }
        public API.Models.Contestant Contestant { get; private set; }
        public API.Models.Problem Problem { get; private set; }
        public API.Models.Test Test { get; private set; }
        public API.Models.SubmissionResult Result { get; private set; }
        public API.Models.Status Status { get; private set; }

        public EvaluationResultEventArgs(API.Models.Contest contest, API.Models.Contestant contestant, API.Models.Problem problem, API.Models.Test test, API.Models.SubmissionResult result, API.Models.Status status)
        {
            Contest = contest;
            Contestant = contestant;
            Problem = problem;
            Test = test;
            Result = result;
            Status = status;
        }
    }
}
