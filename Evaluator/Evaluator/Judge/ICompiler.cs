﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evaluator.Judge
{
    public interface ICompiler
    {
        bool Compile(string sourcePath, out string executablePath);
    }
}
