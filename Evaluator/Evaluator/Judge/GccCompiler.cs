﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evaluator.Judge
{
    public class GccCompiler : ICompiler
    {
        private string _path;

        public bool Compile(string sourcePath, out string executablePath)
        {
            ProcessStartInfo psi = new ProcessStartInfo();
            string[] parts = sourcePath.Split('.');
            if (parts.Length > 2)
            {
                for (int i = 1; i < parts.Length - 1; i++)
                {
                    parts[0] += "." + parts[i];
                }
            }
            psi.FileName = _path;
            psi.Arguments = " -o " + parts[0] + " " + sourcePath;
            psi.UseShellExecute = false;
            psi.CreateNoWindow = true;
            var p = Process.Start(psi);
            p.WaitForExit(5000);
            if (!p.HasExited)
            {
                p.Kill();                
            }
            executablePath = sourcePath.Remove(sourcePath.LastIndexOf('.')) + Constants.exe;
            return File.Exists(executablePath);
        }

        public GccCompiler(string path)
        {
            _path = path;
        }
    }
}
