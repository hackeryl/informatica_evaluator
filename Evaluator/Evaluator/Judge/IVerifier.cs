﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evaluator.Judge
{
    public interface IVerifier
    {
        API.Models.SubmissionResult Verify(API.Models.Submission submission, API.Models.Problem problem, API.Models.Test test, string OkOutput, ExecutionStatus status);
    }
}
