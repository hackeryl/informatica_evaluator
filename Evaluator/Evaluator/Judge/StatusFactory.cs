﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evaluator.Judge
{
    public class StatusFactory
    {
        private static Dictionary<string, API.Models.Status> statuses = new Dictionary<string, API.Models.Status>();

        public static void RegisterStatus(string code, API.Models.Status status)
        {
            statuses[code] = status;
        }

        public static void RemoveStatus(string code)
        {
            if (statuses[code] != null)
            {
                statuses.Remove(code);
            }
        }

        public static API.Models.Status GetStatus(string code)
        {
            return statuses[code];
        }
    }
}
