﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Evaluator.Judge
{
    public class Sandbox
    {
        private string _workingPath;
        private string _outputPath;
        private string _sandboxStartPath;

        private readonly Type flag = typeof(File);

        public Sandbox(string path)
        {
            _workingPath = path + "\\";
        }

        public Sandbox(string sandboxStartPath, string workingPath, string outputPath)
        {
            _sandboxStartPath = sandboxStartPath;
            _workingPath = workingPath + "\\";
            _outputPath = outputPath;
        }

        public ExecutionStatus Execute(string testsPath, API.Models.Test test, API.Models.Problem problem)
        {
            ExecutionStatus status;
            try
            {
                CopyTestToSandbox(testsPath + test.Name + Constants.In, problem);
                ProcessStartInfo psi = new ProcessStartInfo(_workingPath + problem.Name + Constants.exe);
                psi.WorkingDirectory = _workingPath;
                psi.UseShellExecute = false;
                psi.CreateNoWindow = true;
                Process exec = new Process();
                exec.StartInfo = psi;
                bool hasExited = false;
                double totalMilliseconds = 0;
                lock (flag)
                {                    
                    exec.Start();
                    exec.WaitForExit(problem.TimeLimit + 200);
                    totalMilliseconds = exec.TotalProcessorTime.TotalMilliseconds;
                    if (!(hasExited=exec.HasExited))
                    {
                        exec.CloseMainWindow();
                        exec.Kill();
                    }
                    exec.Dispose();
                    Thread.Sleep(250);                    
                }
                string outputFilePath = _workingPath + problem.Name + Constants.Out;
                status = new ExecutionStatus(hasExited, totalMilliseconds, 0, File.Exists(outputFilePath), GetOutputString(outputFilePath));
                DeleteOutputFile(outputFilePath);
            }
            catch (Exception e)
            {
                status = new ExecutionStatus(false, 0, 0, false, e.Message);
            }
            return status;
        }

        public ExecutionStatus Execute2(string testsPath, API.Models.Test test, API.Models.Problem problem)
        {
            ExecutionStatus status;
            try
            {
                CopyTestToSandbox(testsPath + test.Name + Constants.In, problem);
                ProcessStartInfo psi = new ProcessStartInfo(_workingPath + problem.Name + Constants.exe);
                psi.WorkingDirectory = _workingPath;
                psi.UseShellExecute = false;
                psi.CreateNoWindow = true;
                Process exec = new Process();
                exec.StartInfo = psi;
                bool hasExited = false;
                double totalMilliseconds = 0;
                double memoryUsed = 0;
                lock (flag)
                {
                    exec.Start();
                    var ts = new CancellationTokenSource();
                    Task.Run(() =>
                        {
                            Thread.Sleep(5);
                            try
                            {
                                while (!exec.HasExited)
                                {
                                    try
                                    {
                                        var m = exec.WorkingSet64 / 1024.0 / 1024.0;
                                        if (m > memoryUsed)
                                        {
                                            memoryUsed = m;
                                        }
                                    }
                                    catch (Exception e) { var x = e.Message; }
                                }
                            }
                            catch { }
                        }, ts.Token);                    
                    exec.WaitForExit(problem.TimeLimit + 200);              
                    totalMilliseconds = exec.TotalProcessorTime.TotalMilliseconds;
                    if (!(hasExited = exec.HasExited))
                    {
                        exec.CloseMainWindow();
                        exec.Kill();
                    }
                    ts.Cancel();
                    exec.Dispose();                    
                    Thread.Sleep(250);
                }
                
                string outputFilePath = _workingPath + problem.Name + Constants.Out;
                status = new ExecutionStatus(hasExited, totalMilliseconds, memoryUsed, File.Exists(outputFilePath), GetOutputString(outputFilePath));
                DeleteOutputFile(outputFilePath);
            }
            catch (Exception e)
            {
                status = new ExecutionStatus(false, 0, 0, false, e.Message);
            }
            return status;
        }

        public ExecutionStatus ExecuteInSandbox(string testsPath, API.Models.Test test, API.Models.Problem problem)
        {
            ExecutionStatus status;
            try
            {
                CopyTestToSandbox(testsPath + test.Name + Constants.In, problem);
                //ProcessStartInfo psi = new ProcessStartInfo(_sandboxStartPath + " " + _workingPath + problem.Name + Constants.exe);
                ProcessStartInfo psi = new ProcessStartInfo(_sandboxStartPath, "/wait /silent /hide_window " + _workingPath + problem.Name + Constants.exe);
                psi.WorkingDirectory = _workingPath;
                psi.UseShellExecute = false;
                psi.CreateNoWindow = true;
                Process exec = new Process();
                exec.StartInfo = psi;
                bool hasExited = false;
                double totalMilliseconds = 0;
                lock (flag)
                {
                    exec.Start();
                    exec.WaitForExit(problem.TimeLimit + 1000);
                    totalMilliseconds = exec.TotalProcessorTime.TotalMilliseconds;
                    if (!(hasExited = exec.HasExited))
                    {
                        exec.CloseMainWindow();
                        exec.Kill();
                    }
                    exec.Dispose();
                    Process.Start(_sandboxStartPath, "/terminate_all");
                    Thread.Sleep(250);
                }
                string outputFilePath = _outputPath + problem.Name + Constants.Out;
                status = new ExecutionStatus(hasExited, totalMilliseconds, 0, File.Exists(outputFilePath), GetOutputString(outputFilePath));
                DeleteOutputFile(outputFilePath);
            }
            catch (Exception e)
            {
                status = new ExecutionStatus(false, 0, 0, false, e.Message);
            }
            return status;
        }

        private void CopyTestToSandbox(string testPath, API.Models.Problem problem)
        {
            try
            {
                lock (flag)
                {
                    File.Copy(testPath, _workingPath + problem.Name + Constants.In, true);
                }
            }
            catch (Exception)
            {
            }
        }

        private void DeleteOutputFile(string outputFilePath)
        {
            try
            {
                lock (flag)
                {
                    File.Delete(outputFilePath);
                }                
            }
            catch (Exception)
            {
            }
        }

        private string GetOutputString(string path)
        {
            try
            {
                lock (flag)
                {
                    return File.Exists(path) ? File.ReadAllText(path) : String.Empty;
                }
            }
            catch (Exception)
            {
                return String.Empty;
            }            
        }
    }
}
