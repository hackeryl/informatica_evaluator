﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evaluator.Judge
{
    public class OutputMatchVerifier : IVerifier
    {
        public API.Models.SubmissionResult Verify(API.Models.Submission submission, API.Models.Problem problem, API.Models.Test test, string okOutput, ExecutionStatus status)
        {
            API.Models.SubmissionResult result = new API.Models.SubmissionResult();
            result.ID_Submission = submission.ID;
            result.ID_Test = test.ID;
            result.Time = (decimal)status.TotalMilliseconds;
            result.Memory = (decimal)status.UsedMemory;
            if (problem.TimeLimit + 199 < status.TotalMilliseconds)
            {
                result.Score = 0;
                result.ID_Status = StatusFactory.GetStatus(Constants.Statuses.Codes.TLE).ID;
            }
            else if (problem.MemoryLimit < status.UsedMemory)
            {
                result.Score = 0;
                result.ID_Status = StatusFactory.GetStatus(Constants.Statuses.Codes.MLE).ID;
            }
            else if (!status.HasOutputFile)
            {
                result.Score = 0;
                result.ID_Status = StatusFactory.GetStatus(Constants.Statuses.Codes.MOF).ID;
            }
            else if (MathcesOutput(okOutput, status.Output))
            {
                result.Score = test.Points;
                result.ID_Status = StatusFactory.GetStatus(Constants.Statuses.Codes.OK).ID;
            }
            else
            {
                result.Score = 0;
                result.ID_Status = StatusFactory.GetStatus(Constants.Statuses.Codes.WA).ID;
            }
            if (!status.HasExited && result.Score == 0)
            {
                result.Score = 0;
                result.ID_Status = StatusFactory.GetStatus(Constants.Statuses.Codes.TLE).ID;
            }            
            return result;
        }

        private bool MathcesOutput(string okOutput, string executionOutput)
        {
            executionOutput = executionOutput.Replace(" ", ""); 
            executionOutput = executionOutput.Replace("\t", "");
            executionOutput = executionOutput.Replace("\n", ""); 
            executionOutput = executionOutput.Replace("\r", "");
            okOutput = okOutput.Replace(" ", "");
            okOutput = okOutput.Replace("\t", "");
            okOutput = okOutput.Replace("\n", "");
            okOutput = okOutput.Replace("\r", "");
            return okOutput == executionOutput;
        }
    }
}
