﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evaluator.Judge
{
    public class FpcCompiler : ICompiler
    {
        private string _path;

        public bool Compile(string sourcePath, out string executablePath)
        {
            ProcessStartInfo psi = new ProcessStartInfo(_path, sourcePath);
            psi.UseShellExecute = false;
            psi.CreateNoWindow = true;
            var p = Process.Start(psi);
            p.WaitForExit(5000);
            if (!p.HasExited)
            {
                p.Kill();
            }
            executablePath = sourcePath.Remove(sourcePath.LastIndexOf('.')) + Constants.exe;
            return File.Exists(executablePath);
        }

        public FpcCompiler(string path)
        {
            _path = path;
        }
    }
}
