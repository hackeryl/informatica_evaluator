﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evaluator.Judge
{
    public class ExecutionStatus
    {
        public bool HasExited { get; private set; }
        public double TotalMilliseconds { get; private set; }
        public double UsedMemory { get; private set; }
        public string Output { get; private set; }
        public bool HasOutputFile { get; private set; }

        public ExecutionStatus(bool hasExited, double totalMilliseconds, double usedMemory, bool hasOutputFile, string outputString)
        {
            HasExited = hasExited;
            TotalMilliseconds = totalMilliseconds;
            UsedMemory = usedMemory;
            HasOutputFile = hasOutputFile;
            Output = outputString;
        }
    }
}
