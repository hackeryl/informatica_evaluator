﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Evaluator.API;
using System.IO;

namespace Evaluator.Judge
{
    public class Judge
    {
        public delegate void TestEvaluatedEvent(object clock, EvaluationResultEventArgs args);
        public TestEvaluatedEvent TestEvaluated { get; set; }
        public delegate void JudgeNotificationEvent(object clock, JudgeNotificationEventArgs args);
        public JudgeNotificationEvent JudgeNotification { get; set; }

        private string ApiUrl;
        private string ApiKey;
        private string EvaluationPath;
        private string GccCompilerPath;
        private string FpcCompilerPath;
        private DirectoryInfo EvaluationDirectory;
        private DirectoryInfo SubmissionsDirectory;
        private DirectoryInfo TestsDirectory;
        private DirectoryInfo SandboxDirectory;
        private Dictionary<int, string> submissionsPaths = new Dictionary<int, string>();
        private Dictionary<int, string> testsInPaths = new Dictionary<int, string>();
        private Dictionary<int, string> testsOkPaths = new Dictionary<int, string>();
        private Dictionary<int, string> testsOkValues = new Dictionary<int, string>();
        private Repository repository = new Repository();
        private Sandbox sandbox;
        private IVerifier verifier;
        private CodEvalApiClient apiClient;

        public Judge(string apiUrl, string apiKey, string evaluationPath, string gccCompilerPath, string fpcCompilerPath)
        {
            ApiUrl = apiUrl;
            ApiKey = apiKey;
            EvaluationPath = evaluationPath;
            GccCompilerPath = gccCompilerPath;
            FpcCompilerPath = fpcCompilerPath;
            CompilerFactory.RegisterCompiler(Constants.cpp, new GccCompiler(GccCompilerPath));
            CompilerFactory.RegisterCompiler(Constants.pas, new FpcCompiler(FpcCompilerPath));
            CompilerFactory.RegisterCompiler(Constants.c, new GccCompiler(GccCompilerPath));
            EvaluationDirectory = new DirectoryInfo(EvaluationPath);
            SubmissionsDirectory = new DirectoryInfo(EvaluationPath + "\\Submissions");
            TestsDirectory = new DirectoryInfo(EvaluationPath + "\\Tests");
            SandboxDirectory = new DirectoryInfo(EvaluationPath + "\\Sandbox");
            //sandbox = new Sandbox(SandboxDirectory.FullName);
            sandbox = new Sandbox(@"C:\Program Files\Sandboxie\Start.exe", SandboxDirectory.FullName, @"C:\Sandbox\Basilio\DefaultBox\user\current\Desktop\sandbox\Sandbox\");
            verifier = new OutputMatchVerifier();
            apiClient = new CodEvalApiClient(ApiUrl, ApiKey);
        }

        public void Synchronize()
        {
            FireNotification(Constants.Notifications.Syncronizing);
            FireNotification(Constants.Notifications.GetContestsStarted);
            repository.Contests = apiClient.Contests.Get();
            FireNotification(Constants.Notifications.GetContestsEnded);
            FireNotification(Constants.Notifications.GetProblemsStarted);
            repository.Problems = apiClient.Problems.Get();
            FireNotification(Constants.Notifications.GetProblemsEnded);
            FireNotification(Constants.Notifications.GetContestantsStarted);
            repository.Contestants = apiClient.Contestants.Get();
            FireNotification(Constants.Notifications.GetContestantsEnded);
            FireNotification(Constants.Notifications.GetSubmissionsStarted);
            repository.Submissions = apiClient.Submissions.GetByStatus(Evaluator.API.Models.SubmissionStatus.Submitted, Evaluator.API.Models.SubmissionStatus.InQueue);
            FireNotification(Constants.Notifications.GetSubmissionsEnded);
            FireNotification(Constants.Notifications.GetStatusesStarted);
            repository.Statuses = apiClient.Statuses.Get();
            FireNotification(Constants.Notifications.GetStatusesEnded);
            FireNotification(Constants.Notifications.GetTestsStarted);
            repository.Tests = apiClient.Tests.Get();
            FireNotification(Constants.Notifications.GetTestsEnded);

            FireNotification(Constants.Notifications.GetSubmissionsFilesStarted);
            GetSubmissionsFiles();
            FireNotification(Constants.Notifications.GetSubmissionsFilesEnded);
            FireNotification(Constants.Notifications.GetTestsFilesStarted);
            GetTestsFiles();
            FireNotification(Constants.Notifications.GetTestsFilesEnded);
            FireNotification(Constants.Notifications.GetSandboxStarted);
            GetSandbox();
            FireNotification(Constants.Notifications.GetSandboxEnded);
            FireNotification(Constants.Notifications.RegisterStatusesStarted);
            RegisterStatuses();
            FireNotification(Constants.Notifications.RegisterStatusesEnded);
        }

        public void SynchronizeSubmissions()
        {
            FireNotification(Constants.Notifications.GetSubmissionsStarted);
            repository.Submissions = apiClient.Submissions.GetByStatus(Evaluator.API.Models.SubmissionStatus.Submitted, Evaluator.API.Models.SubmissionStatus.InQueue);
            FireNotification(Constants.Notifications.GetSubmissionsEnded);

            FireNotification(Constants.Notifications.GetSubmissionsFilesStarted);
            GetSubmissionsFiles();
            FireNotification(Constants.Notifications.GetSubmissionsFilesEnded);
        }

        public void Evaluate()
        {
            FireNotification(Constants.Notifications.EvaluationBegin);
            repository.SubmissionsResults = new List<API.Models.SubmissionResult>();
            ICompiler compiler = null;
            string executablePath = String.Empty;
            string testsFolder = String.Empty;
            foreach (var contest in repository.Contests)
            {
                foreach (var problem in repository.Problems.Where(a => a.Id_Contest == contest.ID))
                {
                    foreach (var submission in repository.Submissions.Where(a => a.ID_Problem == problem.ID))
                    {
                        compiler = CompilerFactory.GetCompiler(submission.FileName.Remove(0, submission.FileName.LastIndexOf('.')));
                        if (compiler != null && compiler.Compile(submissionsPaths[submission.ID], out executablePath))
                        {
                            testsFolder = TestsDirectory.FullName + "\\" + submission.ID_Problem + "\\";
                            File.Copy(executablePath, SandboxDirectory.FullName + "\\" + problem.Name + Constants.exe, true);
                            foreach (var test in repository.Tests.Where(a => a.ID_Problem == submission.ID_Problem))
                            {
                                ExecutionStatus status = sandbox.Execute2(testsFolder, test, problem);
                                //ExecutionStatus status = sandbox.ExecuteInSandbox(testsFolder, test, problem);
                                API.Models.SubmissionResult result = verifier.Verify(submission, problem, test, testsOkValues[test.ID], status);
                                //result.ID_Contestant = submission.ID_Contestant;
                                repository.SubmissionsResults.Add(result);
                                if (TestEvaluated != null)
                                {
                                    var contestant = repository.Contestants.FirstOrDefault(a => a.ID == submission.ID_Contestant);
                                    TestEvaluated(this, new EvaluationResultEventArgs(contest, contestant, problem, test, result, repository.Statuses.FirstOrDefault(a => a.ID == result.ID_Status)));
                                }
                            }
                        }
                        else
                        {
                            AddDidNotCompileResults(submission);
                        }
                    }
                }
            }
            FireNotification(Constants.Notifications.EvaluationEnded);
        }

        public bool UpdateResults()
        {
            FireNotification(Constants.Notifications.UpdateRestulStarted);
            bool result = apiClient.SubmissionsResults.PostAsync(repository.SubmissionsResults);
            FireNotification(Constants.Notifications.UpdateResultsEnded);
            return result;
        }

        private void AddDidNotCompileResults(API.Models.Submission submission)
        {
            foreach (var test in repository.Tests.Where(a => a.ID_Problem == submission.ID_Problem))
            {
                API.Models.SubmissionResult result = new API.Models.SubmissionResult();
                //result.ID_Contestant = submission.ID_Contestant;
                //result.ID_Problem = submission.ID_Problem;
                result.ID_Submission = submission.ID;
                result.ID_Test = test.ID;
                result.Memory = 0;
                result.Score = 0;
                result.Time = 0;
                result.ID_Status = StatusFactory.GetStatus(Constants.Statuses.Codes.DNC).ID;
                repository.SubmissionsResults.Add(result);
                if (TestEvaluated != null)
                {
                    var problem = repository.Problems.FirstOrDefault(a => a.ID == submission.ID_Problem);
                    var contest = repository.Contests.FirstOrDefault(a => a.ID == problem.Id_Contest);
                    var contestant = repository.Contestants.FirstOrDefault(a => a.ID == submission.ID_Contestant);
                    TestEvaluated(this, new EvaluationResultEventArgs(contest, contestant, problem, test, result, StatusFactory.GetStatus(Constants.Statuses.Codes.DNC)));
                }
            }
        }

        private void GetTestsFiles()
        {
            if (!TestsDirectory.Exists)
            {
                TestsDirectory.Create();
            }
            else
            {
                Directory.Delete(TestsDirectory.FullName, true);
                TestsDirectory.Create();
            }
            CreateTestsSubdirectories();
            var validTests = new List<API.Models.Test>();
            foreach (var test in repository.Tests)
            {
                string testPath = TestsDirectory.FullName + "\\" + test.ID_Problem + "\\" + test.Name;
                string testInPath = testPath + Constants.In;
                string testOkPath = testPath + Constants.Ok;
                var inBytes = apiClient.Tests.GetTestInputFile(test.ID);
                var okBytes = apiClient.Tests.GetTestOKFile(test.ID);
                if (inBytes != null && okBytes != null)
                {
                    validTests.Add(test);
                    File.WriteAllBytes(testInPath, inBytes);
                    File.WriteAllBytes(testOkPath, okBytes);
                    testsInPaths[test.ID] = testInPath;
                    testsOkPaths[test.ID] = testOkPath;
                    testsOkValues[test.ID] = Encoding.UTF8.GetString(okBytes);                    
                }                               
            }
            repository.Tests = validTests;
        }

        private void CreateTestsSubdirectories()
        {
            string testsPath = TestsDirectory.FullName + "\\";
            foreach (var test in repository.Tests)
            {
                Directory.CreateDirectory(testsPath + test.ID_Problem);
            }
        }

        private void GetSubmissionsFiles()
        {
            if (!SubmissionsDirectory.Exists)
            {
                SubmissionsDirectory.Create();
            }
            else
            {
                Directory.Delete(SubmissionsDirectory.FullName, true);
                SubmissionsDirectory.Create();
            }
            CreateSubmissionsSubdirectories();
            var validSubmissions = new List<API.Models.Submission>();
            foreach (var sol in repository.Submissions)
            {
                var problem = repository.Problems.FirstOrDefault(a => a.ID == sol.ID_Problem);
                string submissionPath = SubmissionsDirectory.FullName + "\\" + problem.Id_Contest + "\\" + sol.ID_Contestant + "\\" + sol.FileName;
                var submissionBytes = apiClient.Submissions.GetSubmissionFile(sol.ID);
                if (submissionBytes != null)
                {
                    File.WriteAllBytes(submissionPath, submissionBytes);
                    submissionsPaths[sol.ID] = submissionPath;
                    validSubmissions.Add(sol);
                }                
            }
            repository.Submissions = validSubmissions;
        }

        private void CreateSubmissionsSubdirectories()
        {
            foreach (var contest in repository.Contests)
            {
                string contestPath = SubmissionsDirectory.FullName + "\\" + contest.ID + "\\";
                Directory.CreateDirectory(contestPath);
                foreach (var contestant in repository.Contestants)
                {
                    Directory.CreateDirectory(contestPath + contestant.ID);
                }
            }
        }

        private void GetSandbox()
        {
            if (!SandboxDirectory.Exists)
            {
                SandboxDirectory.Create();
            }
            else
            {
                Directory.Delete(SandboxDirectory.FullName, true);
                SandboxDirectory.Create();
            }
        }

        private void RegisterStatuses()
        {
            foreach (var status in repository.Statuses)
            {
                StatusFactory.RegisterStatus(status.Code, status);
            }
        }

        private void FireNotification(string message)
        {
            if (JudgeNotification != null)
            {
                JudgeNotification(this, new JudgeNotificationEventArgs(message, DateTime.Now));
            }
        }
    }
}
